import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import {
	Hero,
	Container,
	Section,
	Heading,
	Navbar
} from "react-bulma-components";
import About from "./components/About";
import Contact from "./components/Contact";
import Projects from "./components/Projects";
import HeroFooter from "./components/HeroFooter";
import Landing from "./components/Landing";

import Typewriter from "typewriter-effect";
function App() {
	const [isActive, setIsActive] = useState(false);
	const [navLink, setNavLink] = useState("home");
	const [burgerOpen, setBurgerOpen] = useState(false);
	const homeActive = () => {
		setIsActive(true);
		setNavLink("home");
	};

	const aboutActive = () => {
		setIsActive(true);
		setNavLink("about");
	};

	const projectsActive = () => {
		setIsActive(true);
		setNavLink("projects");
	};

	const contactActive = () => {
		setIsActive(true);
		setNavLink("contact");
	};
	return (
		<div>
			<Navbar active={burgerOpen} color="dark" fixed="top">
				<Navbar.Brand>
					<Navbar.Burger
						className="active"
						onClick={() => {
							setBurgerOpen(!burgerOpen);
						}}
					/>
				</Navbar.Brand>
				<Navbar.Menu>
					<Navbar.Container>
						<Navbar.Item
							href="#home"
							active={
								isActive && navLink === "home" ? true : false
							}
							onClick={homeActive}
						>
							Home
						</Navbar.Item>

						<Navbar.Item
							href="#about"
							active={
								isActive && navLink === "about" ? true : false
							}
							onClick={aboutActive}
						>
							About
						</Navbar.Item>
						<Navbar.Item
							href="#projects"
							active={
								isActive && navLink === "projects"
									? true
									: false
							}
							onClick={projectsActive}
						>
							Projects
						</Navbar.Item>
						<Navbar.Item
							href="#contact"
							active={
								isActive && navLink === "contact" ? true : false
							}
							onClick={contactActive}
						>
							Contact Me
						</Navbar.Item>
						<Navbar.Item
							target="_blank"
							href="https://docs.google.com/document/d/1nGBsup_evnE1351d8Mg1M2actsIM_4Rbla1PvkdNgJw/edit?usp=sharing"
						>
							My Resume
						</Navbar.Item>
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
			<Hero id="home" size="fullheight" color="dark" gradient>
				<Landing />
			</Hero>

			<Hero id="about" size="fullheight" className="aboutMe">
				<About />
			</Hero>

			<Hero id="projects" gradient className="projectsMe">
				<Projects />
			</Hero>

			<Hero id="contact" size="fullheight" color="dark" gradient>
				<Hero.Body>
					<Contact />
				</Hero.Body>
			</Hero>
		</div>
	);
}

export default App;
