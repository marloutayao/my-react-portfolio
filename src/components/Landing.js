import React from "react";
import Typewriter from "typewriter-effect";
import { Hero, Container, Section, Heading } from "react-bulma-components";

const Landing = props => {
	return (
		<div>
			<Hero>
				<main>
					<div className="intro">
						<Typewriter
							options={{
								strings: [
									`Hello, I'm Marlou Tayao`,
									`I'm a full-stack web developer`
								],
								autoStart: true,
								loop: true,
								delay: 50,
								cursor: ""
							}}
						/>
					</div>
					<div className="tagline"></div>
					<div className="icons-social">
						<a
							target="_blank"
							href="https://gitlab.com/marloutayao"
						>
							<i className="fab fa-github"></i>
						</a>
						<a
							target="_blank"
							href="https://www.linkedin.com/in/marlou-tayao-931264194/"
						>
							<i className="fab fa-linkedin"></i>
						</a>
					</div>
				</main>
			</Hero>
		</div>
	);
};

export default Landing;
