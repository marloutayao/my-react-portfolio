import React, { useState } from "react";
import { Hero, Tabs } from "react-bulma-components";

const HeroFooter = props => {
	const [visible, setVisible] = useState(false);
	const [isActive, setIsActive] = useState("");
	const showAbout = () => {
		setVisible(true);
		setIsActive("about");
		props.about();
	};
	const showProjects = () => {
		setVisible(true);
		setIsActive("projects");
		props.projects();
	};
	const showContact = () => {
		setVisible(true);
		setIsActive("contacts");
		props.contact();
	};

	return (
		<Hero.Footer>
			<Tabs fullwidth={true} className="is-boxed">
				<Tabs.Tab
					active={visible && isActive === "about" ? true : false}
					onClick={showAbout}
				>
					About Me
				</Tabs.Tab>
				<Tabs.Tab
					active={visible && isActive === "projects" ? true : false}
					onClick={showProjects}
				>
					Projects
				</Tabs.Tab>
				<Tabs.Tab
					active={visible && isActive === "contacts" ? true : false}
					onClick={showContact}
				>
					Contact
				</Tabs.Tab>
			</Tabs>
		</Hero.Footer>
	);
};
export default HeroFooter;
