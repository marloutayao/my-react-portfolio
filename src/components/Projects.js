import React, { useState } from "react";
import {
	Container,
	Heading,
	Card,
	Media,
	Image,
	Content,
	Columns
} from "react-bulma-components";
import ScrollAnimation from "react-animate-on-scroll";
const Projects = props => {
	return (
		<Container>
			<hr />
			<Columns>
				<Columns.Column size={4}>
					<ScrollAnimation animateIn="zoomIn">
						<Card>
							<Card.Image
								size="4by3"
								src="./images/autoleague.png"
							/>
							<Card.Header>
								<Card.Header.Title>
									AutoLeague
								</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<Content>
									A static website for car showroom made using
									HTML, CSS, and Bootstrap
								</Content>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="https://marloutayao.gitlab.io/autoleague/"
								>
									View
								</Card.Footer.Item>
								<Card.Footer.Item
									target="_blank"
									renderAs="a"
									href="https://gitlab.com/marloutayao/autoleague"
								>
									Gitlab
								</Card.Footer.Item>
							</Card.Footer>
						</Card>
					</ScrollAnimation>
				</Columns.Column>
				<Columns.Column size={4}>
					<ScrollAnimation animateIn="zoomIn">
						<Card>
							<Card.Image
								size="4by3"
								src="./images/mtrentals.png"
							/>
							<Card.Header>
								<Card.Header.Title>MTRentals</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<Content>
									A asset management made using PHP, Laravel,
									Bootstrap and Semantic UI
								</Content>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="https://mtrentals.herokuapp.com/"
								>
									View
								</Card.Footer.Item>
								<Card.Footer.Item
									target="_blank"
									renderAs="a"
									href="https://gitlab.com/marloutayao/capstone2-mtr"
								>
									Gitlab
								</Card.Footer.Item>
							</Card.Footer>
						</Card>
					</ScrollAnimation>
				</Columns.Column>
				<Columns.Column size={4}>
					<ScrollAnimation animateIn="zoomIn">
						<Card>
							<Card.Image
								size="4by3"
								src="./images/emarifotos.png"
							/>
							<Card.Header>
								<Card.Header.Title>
									Emarifotos
								</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<Content>
									A online reservation for emarifotos made
									using Native PHP
								</Content>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="http://emarifotos-2019.000webhostapp.com/"
								>
									View
								</Card.Footer.Item>
								<Card.Footer.Item renderAs="a">
									Not Available
								</Card.Footer.Item>
							</Card.Footer>
						</Card>
					</ScrollAnimation>
				</Columns.Column>

				<Columns.Column size={4}>
					<ScrollAnimation animateIn="zoomIn">
						<Card>
							<Card.Image
								size="4by3"
								src="./images/tindahanq.png"
							/>
							<Card.Header>
								<Card.Header.Title>TindahanQ</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<Content>
									E-Commerce with paypal integration made
									using PHP, Laravel, and Bootstrap
								</Content>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="http://tindahanq.herokuapp.com/"
								>
									View
								</Card.Footer.Item>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="https://gitlab.com/marloutayao/tindahanq"
								>
									GitLab
								</Card.Footer.Item>
							</Card.Footer>
						</Card>
					</ScrollAnimation>
				</Columns.Column>
				<Columns.Column size={4}>
					<ScrollAnimation animateIn="zoomIn">
						<Card>
							<Card.Image
								size="4by3"
								src="./images/mtreservations.png"
							/>
							<Card.Header>
								<Card.Header.Title>
									MTReservations
								</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<Content>
									A reservation website using MongoDB,
									Express.js, React.js, Node.js, and GraphQL
								</Content>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="https://mtreservations.herokuapp.com/"
								>
									View
								</Card.Footer.Item>
								<Card.Footer.Item
									renderAs="a"
									target="_blank"
									href="https://gitlab.com/marloutayao/capstone3-mtr"
								>
									GitLab
								</Card.Footer.Item>
							</Card.Footer>
						</Card>
					</ScrollAnimation>
				</Columns.Column>
			</Columns>
			<hr />
		</Container>
	);
};

export default Projects;
